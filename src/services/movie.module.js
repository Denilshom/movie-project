import ApiService from "@/services/api.service";

//action
export const LIST_MOVIES = "listMovies";
export const SHOW_MOVIE = "showMovie";
export const SEARCH_MOVIE = "searchMovie";
//mutation
export const SET_MOVIES = "setMovies";
export const SET_MOVIE = "setMovie";
export const SET_ERROR = "setError";
export const PURGE_MOVIE = "purgeMovie";
//getter
export const GET_MOVIES = "getMovies";
export const GET_MOVIE = "getMovie";
//other
const url = process.env.VUE_APP_API_BASE_URL;
const api = "api_key=aa75d97511bc79fed32f3a565f315275";
//state
const state = {
    errors: null,
    movies: [],
    movie: ""
};

const getters = {
    [GET_MOVIES](state) {
        return state.movies;
    },
    [GET_MOVIE](state) {
        return state.movie;
    }
};


const actions = {
    [LIST_MOVIES](context, payload) {
        return new Promise((resolve, reject) => {
            ApiService.get(`${url}/3/trending/all/day?${api}&page=${payload}`)
            .then(res => {
                console.log("services", res.data);
                context.commit(SET_MOVIES, res.data);
                resolve(res.data);
            }).catch(e => {
                reject({ data: e?.response?.data });
            });
        })
    },

    [SHOW_MOVIE](context, id) {
        return new Promise((resolve, reject) => {
            ApiService.get(`${url}/3/movie/${id}?${api}`)
            .then(res => {
                console.log("services", res.data);
                context.commit(SET_MOVIE, res.data);
                resolve(res.data);
            }).catch(e => {
                reject({ data: e?.response?.data });
            });
        })
    },
    [SEARCH_MOVIE](context, payload) {
        return new Promise((resolve, reject) => {
            console.log("payload", payload.query)
            ApiService.get(`${url}/3/search/movie?${api}&query=${payload.query}&page=${payload.page}`)
            .then(res => {
                console.log("searc:",res.data)
                context.commit(SET_MOVIES, res.data);
                resolve(res.data);
            }).catch(e => {
                reject({ data: e?.response?.data });
            });
        })
    },
};

const mutations = {
    [SET_MOVIES](state, movies) {
        state.movies = movies;
        state.errors = {};
    },
    [SET_MOVIE](state, movie) {
        state.movie = movie;
    },
    [SET_ERROR](state, error) {
        state.errors = error;
    },
    [PURGE_MOVIE](state) {
        state.movie = [];
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};