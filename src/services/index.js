import Vue from 'vue'
import Vuex from 'vuex'

import movies from "./movie.module";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    movies
  }
});
