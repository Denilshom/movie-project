import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";

const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = process.env.VUE_APP_BASE_URL;
    },
    get(resource, params = null) {
        return Vue.axios.get(`${resource}`, params);
    },
};

export default ApiService;